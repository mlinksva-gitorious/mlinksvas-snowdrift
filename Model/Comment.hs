module Model.Comment
    ( ClosureMap
    , FlagMap
    , TicketMap
    , approveComment
    , buildCommentForest
    , buildCommentTree
    , canDeleteComment
    , canEditComment
    , deleteComment
    , editComment
    , flagComment
    , getAllClosedRootComments
    , getAllOpenRootComments
    , getAllRootComments
    , getAncestorClosures
    , getAncestorClosures'
    , getCommentAncestors
    , getCommentDepth
    , getCommentDepth404
    , getCommentDescendants
    , getCommentDescendantsIds
    , getCommentDestination
    , getCommentFlagging
    , getCommentsDescendants
    , getCommentPage
    , getCommentPageId
    , getCommentPageEntity'
    , getCommentRethread
    , getCommentTags
    , getCommentsUsers
    , getTags
    , insertApprovedComment
    , insertUnapprovedComment
    , isApproved
    , isEvenDepth
    , isFlagged
    , isOddDepth
    , isTopLevel
    , makeClosureMap
    , makeFlagMap
    , makeModeratedComment
    , makeTicketMap
    , newClosedCommentClosure
    , newRetractedCommentClosure
    , rethreadComments
    , subGetCommentAncestors
    ) where

import Import

import           Model.Comment.Sql
import           Model.Message

import qualified Control.Monad.State         as St
import           Control.Monad.Writer.Strict (tell)
import           Data.Foldable               (Foldable)
import qualified Data.Foldable               as F
import qualified Data.Map                    as M
import           Data.Maybe                  (fromJust)
import qualified Data.Set                    as S
import qualified Data.Text                   as T
import           Data.Tree
import           GHC.Exts                    (IsList(..))
import           Yesod.Markdown              (Markdown(..))

type ClosureMap = Map CommentId CommentClosure
type TicketMap  = Map CommentId (Entity Ticket)
type FlagMap    = Map CommentId (Maybe Markdown, [FlagReason])

approveComment :: UserId -> CommentId -> Comment -> SDB ()
approveComment user_id comment_id comment = do
    lift upd
    tell [ECommentPosted comment_id comment]
  where
    upd = liftIO getCurrentTime >>= \now ->
        update $ \c -> do
        set c [ CommentModeratedTs =. val (Just now)
              , CommentModeratedBy =. val (Just user_id)
              ]
        where_ (c ^. CommentId ==. val comment_id)

insertApprovedComment :: UTCTime
                      -> UTCTime
                      -> UserId
                      -> DiscussionId
                      -> Maybe CommentId
                      -> UserId
                      -> Markdown
                      -> Int
                      -> SDB CommentId
insertApprovedComment created_ts moderated_ts moderated_by discussion_id mparent_id user_id text depth =
    insertComment
      (Just moderated_ts)
      (Just moderated_by)
      ECommentPosted
      created_ts
      discussion_id
      mparent_id
      user_id
      text
      depth

insertUnapprovedComment :: UTCTime
                        -> DiscussionId
                        -> Maybe CommentId
                        -> UserId
                        -> Markdown
                        -> Int
                        -> SDB CommentId
insertUnapprovedComment = insertComment Nothing Nothing ECommentPending

insertComment :: Maybe UTCTime
              -> Maybe UserId
              -> (CommentId -> Comment -> SnowdriftEvent)
              -> UTCTime
              -> DiscussionId
              -> Maybe CommentId
              -> UserId
              -> Markdown
              -> Int
              -> SDB CommentId
insertComment mmoderated_ts mmoderated_by mk_event created_ts discussion_id mparent_id user_id text depth = do
    let comment = Comment
                    created_ts
                    mmoderated_ts
                    mmoderated_by
                    discussion_id
                    mparent_id
                    user_id
                    text
                    depth
    comment_id <- lift $ insert comment
    tell [mk_event comment_id comment]
    return comment_id

isApproved :: Comment -> Bool
isApproved = isJust . commentModeratedTs

isTopLevel :: Comment -> Bool
isTopLevel = (== 0) . commentDepth

isEvenDepth :: Comment -> Bool
isEvenDepth comment = not (isTopLevel comment) && commentDepth comment `mod` 2 == 1

isOddDepth :: Comment -> Bool
isOddDepth comment = not (isTopLevel comment) && not (isEvenDepth comment)

isFlagged :: CommentId -> DB Bool
isFlagged = fmap (maybe False (const True)) . getBy . UniqueCommentFlagging

-- | Build a tree of comments, given the root and replies. The replies are not necessarily
-- direct or indirect descendants of the root, but rather may be siblings, nephews, etc.
-- This is done to greatly simplify the calling code of this function.
--
-- THIS FUNCTION RELIES ON THE SORT ORDER OF THE REPLIES! Specifically, they must be sorted
-- in ascending-parent-id major, ascending-timestamp minor order.
buildCommentTree :: (Entity Comment, [Entity Comment]) -> Tree (Entity Comment)
buildCommentTree = unfoldTree step
  where
    step :: (Entity Comment, [Entity Comment]) -> (Entity Comment, [(Entity Comment, [Entity Comment])])
    step (root, replies) = (root, children_and_their_descendants)
      where
        descendants :: [Entity Comment]
        descendants = dropWhile (not . isParentOf root) replies

        -- children :: [Entity Comment]
        -- children_descendants :: [Entity Comment]
        (children, children_descendants) = span (isParentOf root) descendants

        children_and_their_descendants :: [(Entity Comment, [Entity Comment])]
        children_and_their_descendants = map (, children_descendants) children

        isParentOf :: Entity Comment -> Entity Comment -> Bool
        isParentOf (Entity parent_key _) (Entity _ child) = Just parent_key == commentParent child

buildCommentForest :: [Entity Comment]                                             -- root comments
                   -> [Entity Comment]                                             -- replies comments
                   -> Forest (Entity Comment)
buildCommentForest roots replies = (map (buildCommentTree . (, replies))) roots

canDeleteComment :: UserId -> Entity Comment -> DB Bool
canDeleteComment user_id (Entity comment_id comment) = do
    if commentUser comment /= user_id
        then return False
        else do
          descendants_ids <- getCommentDescendantsIds comment_id
          if null descendants_ids
              then return True
              else return False

canEditComment :: UserId -> Comment -> Bool
canEditComment user_id = (user_id ==) . commentUser

-- | Delete-cascade a comment from the database.
deleteComment :: CommentId -> DB ()
deleteComment = deleteCascade

-- | Edit a comment's text. If the comment was flagged, unflag it and send a
-- message to the flagger.
editComment :: CommentId -> Markdown -> SYDB ()
editComment comment_id text = do
    lift updateCommentText
    lift (getCommentFlagging comment_id) >>= \case
        Nothing -> return ()
        Just (Entity comment_flagging_id CommentFlagging{..}) -> do
            let permalink_route = DiscussCommentR
                                    commentFlaggingProjectHandle
                                    commentFlaggingTarget
                                    comment_id
            permalink_text <- lift $ getUrlRender <*> pure permalink_route
            let message_text = Markdown $ "A comment you flagged has been edited and reposted to the site. You can view it [here](" <> permalink_text <> ")."
            lift $ deleteCascade comment_flagging_id -- delete flagging and all flagging reasons with it.
            snowdrift_id <- lift getSnowdriftId
            insertMessage_ MessageDirect (Just snowdrift_id) Nothing (Just $ commentFlaggingFlagger) message_text True
  where
    updateCommentText =
        update $ \c -> do
        set c [ CommentText =. val text ]
        where_ (c ^. CommentId ==. val comment_id)

-- | Flag a comment. Send a message to the poster about the flagging. Return whether
-- or not the flag was successful (fails if the comment was already flagged.)
flagComment :: Text -> Text -> CommentId -> Text -> UserId -> [FlagReason] -> Maybe Markdown -> SYDB Bool
flagComment project_handle target comment_id permalink_route flagger_id reasons message = do
    poster_id <- lift $ commentUser <$> get404 comment_id
    now <- liftIO getCurrentTime
    lift (insertUnique (CommentFlagging now flagger_id comment_id project_handle target message)) >>= \case
        Nothing -> return False
        Just flagging_id -> do
            lift $ void $ insertMany (map (CommentFlaggingReason flagging_id) reasons)

            let message_text = Markdown . T.unlines $
                    [ "Another user flagged your comment as not meeting the standards of the Code of Conduct. We *want* your involvement as long as it remains respectful and friendly, so please don’t feel discouraged."
                    , ""
                    , "Please follow the link below for clarification and suggestions the flagger may have offered, and take this chance to improve your tone and clarify any misunderstanding. Your newly edited comment will then be publicly visible again."
                    , ""
                    , "Please alert a moderator if you believe that this flagging is inappropriate, if the flagger violated the Code of Conduct in their feedback, or if you want other assistance."
                    , ""
                    , "[link to flagged comment](" <> permalink_route <> ")"
                    ]
            snowdrift_id <- lift getSnowdriftId
            insertMessage_ MessageDirect (Just snowdrift_id) Nothing (Just poster_id) message_text True
            return True

-- | Get all ancestors that have been closed.
getAncestorClosures :: CommentId -> DB [CommentClosure]
getAncestorClosures comment_id = fmap (map entityVal) $
    select $
    from $ \(ca `InnerJoin` cc) -> do
    on_ (ca ^. CommentAncestorAncestor ==. cc ^. CommentClosureComment)
    orderBy [asc (cc ^. CommentClosureComment)]
    where_ (ca ^. CommentAncestorComment ==. val comment_id)
    return cc

-- | Get all ancestors, including this comment, that have been closed.
getAncestorClosures' :: CommentId -> DB [CommentClosure]
getAncestorClosures' comment_id = do
    all_comment_ids <- (comment_id :) <$> getCommentAncestors comment_id
    fmap (map entityVal) $
        select $
        from $ \cc -> do
        where_ (cc ^. CommentClosureComment `in_` valList all_comment_ids)
        return cc

-- | Get a comment's ancestors' ids.
getCommentAncestors :: CommentId -> DB [CommentId]
getCommentAncestors = fmap (map unValue) . select . querAncestors

subGetCommentAncestors :: CommentId -> SqlExpr (ValueList CommentId)
subGetCommentAncestors = subList_select . querAncestors

getCommentDepth :: CommentId -> DB Int
getCommentDepth = fmap commentDepth . getJust

getCommentDepth404 :: CommentId -> Handler Int
getCommentDepth404 = fmap commentDepth . runYDB . get404

-- | Get the CommentFlagging even for this Comment, if there is one.
getCommentFlagging :: CommentId -> DB (Maybe (Entity CommentFlagging))
getCommentFlagging = getBy . UniqueCommentFlagging

-- | Partial function.
getCommentPage :: CommentId -> DB WikiPage
getCommentPage = fmap entityVal . getCommentPageEntity

-- | Partial function.
getCommentPageId :: CommentId -> DB WikiPageId
getCommentPageId = fmap entityKey . getCommentPageEntity

-- | Partial function. Fails if the given Comment is not on a WikiPage, but some
-- other Discussion.
getCommentPageEntity :: CommentId -> DB (Entity WikiPage)
getCommentPageEntity = fmap fromJust . getCommentPageEntity'

-- | Safe version. TODO: Rename above 'unsafeGetCommentPageEntity'
getCommentPageEntity' :: CommentId -> DB (Maybe (Entity WikiPage))
getCommentPageEntity' comment_id = fmap listToMaybe $
    select $
    from $ \(c `InnerJoin` p) -> do
    on_ (c ^. CommentDiscussion ==. p ^. WikiPageDiscussion)
    where_ (c ^. CommentId ==. val comment_id)
    return p

-- | Get the CommentId this CommentId was rethreaded to, if it was.
getCommentRethread :: CommentId -> DB (Maybe CommentId)
getCommentRethread comment_id = fmap unValue . listToMaybe <$> (
    select $
    from $ \cr -> do
    where_ $ cr ^. CommentRethreadOldComment ==. val comment_id
    return $ cr ^. CommentRethreadNewComment)

-- | Get a Comment's CommentTags.
getCommentTags :: CommentId -> DB [Entity CommentTag]
getCommentTags comment_id =
    select $
    from $ \comment_tag -> do
    where_ $ comment_tag ^. CommentTagComment ==. val comment_id
    return comment_tag

-- | Get a Comment's descendants' ids (don't filter hidden or unmoderated comments).
getCommentDescendantsIds :: CommentId -> DB [CommentId]
getCommentDescendantsIds = fmap (map unValue) . select . querDescendants

-- | Get all descendants of the given root comment.
getCommentDescendants :: Maybe UserId -> ProjectId -> CommentId -> DB [Entity Comment]
getCommentDescendants mviewer_id project_id root_id =
    select $
    from $ \c -> do
    where_ $
        c ^. CommentId `in_` subList_select (querDescendants root_id) &&.
        exprPermissionFilter mviewer_id (val project_id) c
    -- DO NOT change ordering here! buildCommentTree relies on it.
    orderBy [asc (c ^. CommentParent), asc (c ^. CommentCreatedTs)]
    return c

-- | Get all descendants of all given root comments.
getCommentsDescendants :: Maybe UserId -> ProjectId -> [CommentId] -> DB [Entity Comment]
getCommentsDescendants mviewer_id project_id root_ids =
    select $
    from $ \c -> do
    where_ $
        c ^. CommentId `in_` subList_select (querAllDescendants root_ids) &&.
        exprPermissionFilter mviewer_id (val project_id) c
    -- DO NOT change ordering here! buildCommentTree relies on it.
    orderBy [asc (c ^. CommentParent), asc (c ^. CommentCreatedTs)]
    return c

-- | Get the "true" target of this CommentId (which may be itself, if not rethreaded -
-- otherwise, ride the rethread train to the end)
getCommentDestination :: CommentId -> YDB CommentId
getCommentDestination comment_id = do
    void $ get404 comment_id -- make sure the comment even exists, so this function terminates.
    getCommentRethread comment_id >>= maybe (return comment_id) getCommentDestination

-- | Get all Comments on a Discussion that are root comments.
getAllRootComments :: Maybe UserId -> ProjectId -> DiscussionId -> DB [Entity Comment]
getAllRootComments mviewer_id project_id discussion_id =
    select $
    from $ \c -> do
    where_ $
        exprOnDiscussion discussion_id c &&.
        exprRoot c &&.
        exprPermissionFilter mviewer_id (val project_id) c
    return c

getAllClosedRootComments :: Maybe UserId -> ProjectId -> DiscussionId -> DB [Entity Comment]
getAllClosedRootComments mviewer_id project_id discussion_id =
    select $
    from $ \c -> do
    where_ $
        exprOnDiscussion discussion_id c &&.
        exprRoot c &&.
        exprClosed c &&.
        exprPermissionFilter mviewer_id (val project_id) c
    return c

getAllOpenRootComments :: Maybe UserId -> ProjectId -> DiscussionId -> DB [Entity Comment]
getAllOpenRootComments mviewer_id project_id discussion_id =
    select $
    from $ \c -> do
    where_ $
        exprOnDiscussion discussion_id c &&.
        exprRoot c &&.
        exprOpen c &&.
        exprPermissionFilter mviewer_id (val project_id) c
    return c

-- | Get a Comment's Tags.
getTags :: CommentId -> DB [Entity Tag]
getTags comment_id =
    select $
    from $ \(ct `InnerJoin` t) -> do
    on_ (ct ^. CommentTagTag ==. t ^. TagId)
    where_ (ct ^. CommentTagComment ==. val comment_id)
    return t

makeClosureMap :: (IsList c, CommentId ~ Item c) => c -> DB ClosureMap
makeClosureMap comment_ids = fmap (M.fromList . map ((commentClosureComment &&& id) . entityVal)) $
    select $
    from $ \c -> do
    where_ (c ^. CommentClosureComment `in_` valList comment_ids)
    return c

-- Given a collection of CommentId, make a map from CommentId to Entity Ticket. Comments that
-- are not tickets will simply not be in the map.
makeTicketMap :: (IsList c, CommentId ~ Item c) => c -> DB TicketMap
makeTicketMap comment_ids = fmap (M.fromList . map ((ticketComment . entityVal) &&& id)) $
    select $
    from $ \t -> do
    where_ (t ^. TicketComment `in_` valList comment_ids)
    return t

makeFlagMap :: (IsList c, CommentId ~ Item c) => c -> DB FlagMap
makeFlagMap comment_ids = mkFlagMap <$> getCommentFlaggings
  where
    getCommentFlaggings :: DB [(CommentId, Maybe Markdown, FlagReason)]
    getCommentFlaggings = fmap (map unwrapValues) $
        select $
        from $ \(cf `InnerJoin` cfr) -> do
        on_ (cf ^. CommentFlaggingId ==. cfr ^. CommentFlaggingReasonFlagging)
        where_ (cf ^. CommentFlaggingComment `in_` valList comment_ids)
        return (cf ^. CommentFlaggingComment, cf ^. CommentFlaggingMessage, cfr ^. CommentFlaggingReasonReason)

    mkFlagMap :: [(CommentId, Maybe Markdown, FlagReason)] -> FlagMap
    mkFlagMap = foldr (\(comment_id, message, reason) -> M.insertWith combine comment_id (message, [reason])) mempty
      where
        combine :: (Maybe Markdown, [FlagReason]) -> (Maybe Markdown, [FlagReason]) -> (Maybe Markdown, [FlagReason])
        combine (message, reasons1) (_, reasons2) = (message, reasons1 <> reasons2)

newClosedCommentClosure, newRetractedCommentClosure :: MonadIO m => UserId -> Markdown -> CommentId -> m CommentClosure
newClosedCommentClosure    = newCommentClosure Closed
newRetractedCommentClosure = newCommentClosure Retracted

newCommentClosure :: MonadIO m => ClosureType -> UserId -> Markdown -> CommentId -> m CommentClosure
newCommentClosure closure_type user_id reason comment_id =
    (\now -> CommentClosure now user_id closure_type reason comment_id) `liftM` liftIO getCurrentTime

-- | Construct a comment, auto-moderated by 'this' User (because they are established).
makeModeratedComment :: MonadIO m => UserId -> DiscussionId -> Maybe CommentId -> Markdown -> Int -> m Comment
makeModeratedComment user_id discussion_id parent_comment comment_text depth = do
    now <- liftIO getCurrentTime
    return $ Comment
                 now
                 (Just now)
                 (Just user_id)
                 discussion_id
                 parent_comment
                 user_id
                 comment_text
                 depth

-- | Get the set of Users that have posted the given Foldable of comments.
getCommentsUsers :: Foldable f => f (Entity Comment) -> Set UserId
getCommentsUsers = F.foldMap (S.singleton . commentUser . entityVal)

rethreadComments :: RethreadId -> Int -> Maybe CommentId -> DiscussionId -> [CommentId] -> DB [CommentId]
rethreadComments rethread_id depth_offset maybe_new_parent_id new_discussion_id comment_ids = do
    new_comment_ids <- flip St.evalStateT M.empty $ forM comment_ids $ \ comment_id -> do
        rethreads <- St.get

        Just comment <- get comment_id

        let new_parent_id = maybe maybe_new_parent_id Just $ M.lookup (commentParent comment) rethreads

        new_comment_id <- insert $ comment
            { commentDepth = commentDepth comment - depth_offset
            , commentParent = new_parent_id
            , commentDiscussion = new_discussion_id
            }

        St.put $ M.insert (Just comment_id) new_comment_id rethreads

        return new_comment_id

    forM_ (zip comment_ids new_comment_ids) $ \ (comment_id, new_comment_id) -> do
        update $ \ comment_tag -> do
            where_ $ comment_tag ^. CommentTagComment ==. val comment_id
            set comment_tag [ CommentTagComment =. val new_comment_id ]

        update $ \ ticket -> do
            where_ $ ticket ^. TicketComment ==. val comment_id
            set ticket [ TicketComment =. val new_comment_id ]

        insert_ $ CommentRethread rethread_id comment_id new_comment_id

    insertSelect $
        from $ \(comment_closure `InnerJoin` comment_rethread) -> do
        on_ $ comment_closure ^. CommentClosureComment ==. comment_rethread ^. CommentRethreadOldComment
        return $ CommentClosure
                    <#  (comment_closure ^. CommentClosureTs)
                    <&> (comment_closure ^. CommentClosureClosedBy)
                    <&> (comment_closure ^. CommentClosureType)
                    <&> (comment_closure ^. CommentClosureReason)
                    <&> (comment_rethread ^. CommentRethreadNewComment)

    return new_comment_ids
